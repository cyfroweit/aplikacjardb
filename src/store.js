import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        tags: [],
        selected: []
    },
    mutations: {
        pushItem(state, tagName) { //dodanie taga do store
            state.tags.push( { name: tagName, value: false } )
        },
        changeStatus(state, tag) {
            state.selected = []
            state.tags.find((o, i) => {
                if (o.name === tag.name) {
                    state.tags[i] = { name: tag.name, value: !tag.value };
                    return true;
                }
            });

            for (let x = 0; x < state.tags.length; x++) {
                if (state.tags[x].value == true) {
                    state.selected.push(state.tags[x].name)
                }
            }
        }
    },
    actions: {
        addTag ({commit}, tagName) {
            commit('pushItem', tagName)
        },
        toggleStatus({commit}, tagName) {
            commit('changeStatus', tagName)
        }
    },
    getters: {
        getTags: (state) => {
            return state.tags;
        },
        getSelected: (state) => {
            return state.selected
        }
    }
})